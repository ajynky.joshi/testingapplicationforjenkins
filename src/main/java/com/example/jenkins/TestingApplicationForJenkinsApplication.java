package com.example.jenkins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingApplicationForJenkinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestingApplicationForJenkinsApplication.class, args);
	}

}
